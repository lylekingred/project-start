<?php
/*----------------------------------------------------------------*\
	REMOVE ADMIN BAR NODES
\*----------------------------------------------------------------*/
add_filter('autoptimize_filter_toolbar_show','__return_false');

/*----------------------------------------------------------------*\
	REMOVE UNNEEDED DASHBOARD WIDGETS
\*----------------------------------------------------------------*/
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/*----------------------------------------------------------------*\
	DEVELOPER DASHBOARD WIDGET
\*----------------------------------------------------------------*/
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Need Help? Have Questions?', 'custom_dashboard_help');
}
function custom_dashboard_help() {
	echo '<p><img class="headshot" src="/wp-content/uploads/2020/05/lyle-king-red_headshot.png" alt="Lyle King Red your website designer and developer" width="auto" height="120" />When the time comes you need to update your site or have some question on how to use WordPress or just confused feel free to reach out.</p><a href="mailto:hi@lyle.red">hi@lyle.red</a> | <a href="tel:+12489785709">(248) 978-5709</a> | <a target="_blank" href="https://www.lyle.red/">www.lyle.red</a>';
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

/*----------------------------------------------------------------*\
	CUSTOM CSS FOR WP ADMIN AREA
\*----------------------------------------------------------------*/
function my_custom_admin() {
  echo '
		<style>
			#custom_help_widget img.headshot {
				margin: 0;
				position: absolute;
				bottom: 0px;
				right: -24px;
			}
			#custom_help_widget .inside {
				min-height: 120px;
				padding-right: 112px;
				overflow: hidden;
			}
		</style>
	';
}
add_action('admin_head', 'my_custom_admin');