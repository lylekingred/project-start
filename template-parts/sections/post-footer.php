<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div class="copyright">
		<p>Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		<a target="_blank" href="https://www.lyle.red/">👨‍💻👨‍🎨 Design and Development by <span>Lyle King Red</span>.</a>
	</div>
</footer>